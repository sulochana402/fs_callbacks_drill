/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/

const fs = require("fs");
const path = require("path");

// function to create a directory of random JSON files
function createRandomJSONFiles(directory, numberOfFiles, callback) {
    fs.mkdir(directory, (err) => {
        if (err) {
            return callback(err);
        } 
        for (let index = 1; index <= numberOfFiles; index++) {
            const fileName = `file_${index}.json`;
            const filePath = path.join(directory, fileName);
            const data = { file: `${index}` };
            fs.writeFile(filePath, JSON.stringify(data), (err) => {
                if (err) {
                    callback(err);
                } else {
                    console.log(`${fileName} created successfully`);
                }
            });
        }
        callback(null);
    });
}

// function to delete files in a directory
function deleteFiles(directory, numberOfFiles, callback) {
    let deleteCount = 0;
    for (let index = 1; index <= numberOfFiles; index++) {
        const fileName = `file_${index}.json`;
        const filePath = path.join(directory, fileName);
        fs.unlink(filePath, (err) => {
            if (err) {
                callback(err);
            } else {
                console.log(`${fileName} deleted successfully`);
                deleteCount++;
                if (deleteCount === numberOfFiles) {
                    callback(null, "Deleted all files successfully");
                }
            }
        });
    }
}
module.exports = { createRandomJSONFiles, deleteFiles }; 
