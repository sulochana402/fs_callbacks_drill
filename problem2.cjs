/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/
const fs = require("fs");
const path = require("path");
const filePath = path.join(__dirname, "./filenames.txt");

function problem2() {
  fs.readFile(path.join(__dirname,"lipsum.txt"), "utf-8", (err, data) => {
    if (err) {
      console.error(err);
    } else {
      console.log("Read lipsum.txt file");
      let upperCaseData = data.toString().toUpperCase();
      fs.writeFile(path.join(__dirname,"upperCase.txt"), upperCaseData, (err, data) => {
        if (err) {
          console.error(err);
        } else {
          console.log("Converted lipsum file data to uppercase");
          fs.appendFile(filePath, `upperCase.txt\n`, (err) => {
            if (err) {
              console.error("Error appending filename to filenames.txt:", err);
            } else {
              console.log(`Appended upperCase.txt to filenames.txt`);
              fs.readFile(path.join(__dirname,"upperCase.txt"), "utf-8", (err, data) => {
                if (err) {
                  console.error(err);
                } else {
                  console.log("Read upperCase.txt");
                  let lowerCaseData = upperCaseData.toLowerCase();
                  let splitDataLowerCase = lowerCaseData.split(". ").join("\n");
                  fs.writeFile(
                    path.join(__dirname,"lowerCase.txt"),
                    splitDataLowerCase,
                    (err, data) => {
                      if (err) {
                        console.error(err);
                      } else {
                        console.log("created lowerCase.txt file");
                        fs.appendFile(filePath,`lowerCase.txt\n`,(err, data) => {
                            if (err) {
                              console.error(err);
                            } else {
                              console.log(`Appended lowerCase.txt to filenames.txt`);
                              fs.readFile(path.join(__dirname,"lowerCase.txt"),"utf-8",(err, data) => {
                                  if (err) {
                                    console.error(err);
                                  } else {
                                    console.log("Read lowerCase.txt");
                                    let sortedData = data.toString().split("/n")
                                      .filter((sentences) => {
                                        if (sentences) {
                                          return sentences.trim();
                                        }
                                      }).sort().join("\n");
                                    console.log("sorted the data from lowerCase.txt");
                                    fs.writeFile(path.join(__dirname,"sortedData.txt"),sortedData,(err, data) => {
                                        if (err) {
                                          console.error(err);
                                        } else {
                                          console.log("created sortedData.txt file");
                                          fs.appendFile(filePath,`sortedData.txt`,(err) => {
                                              if (err) {
                                                console.error(err);
                                              } else {
                                                console.log(`Appended sortedData.txt to filenames.txt`);
                                                fs.readFile(filePath,"utf-8",(err, data) => {
                                                    if (err) {
                                                      console.error(err);
                                                    } else {
                                                      console.log(
                                                        "Read filenames.txt file"
                                                      );
                                                      data.toString().split("\n").map((file) => {
                                                          fs.unlink(path.join(__dirname,`${file}`),(err) => {
                                                              if (err) {
                                                                console.error(err);
                                                              } else {
                                                                console.log(`${file} deleted`);
                                                                return;
                                                              }
                                                            }
                                                          );
                                                      });
                                                    }
                                                  }
                                                );
                                              }
                                            }
                                          );
                                        }
                                      }
                                    );
                                  }
                                }
                              );
                            }
                          }
                        );
                      }
                    }
                  );
                }
              });
            }
          });
        }
      });
    }
  });
}
module.exports = problem2;
