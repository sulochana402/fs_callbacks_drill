const problem1 = require("../problem1.cjs");

let directory = "./random_files";
let numberOfFiles = 3;

problem1.createRandomJSONFiles(directory, numberOfFiles, (err) => {
  if (err) {
    console.error("Error creating files:", err);
  } else {
    console.log("Files created successfully");
  }

  problem1.deleteFiles(directory, numberOfFiles, (err) => {
    if (err) {
      console.error("Error deleting files:", err);
    } else {
      console.log("Files deleted successfully");
    }
  });
});
